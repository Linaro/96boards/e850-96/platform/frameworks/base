/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.os.epic;

import android.content.Context;
import com.android.server.SystemService;

import android.os.epic.IEpicObject;
import android.os.epic.EpicObject;

/**
 * The epic manager service is responsible for coordinating epic management
 * functions on the device.
 */
public final class EpicManagerService extends SystemService
{
	private static final String TAG = "EpicManagerService";

	private final Context mContext;

	/** @hide */
	public EpicManagerService(Context context) {
		super(context);
		mContext = context;
	}

	/** @hide */
	@Override
	public void onStart() {
		publishBinderService(Context.EPIC_SERVICE, new BinderService());
	}

	/** @hide */
	@Override
	public void onBootPhase(int phase) {
	}

	/** @hide */
	public void systemReady() {
	}

	/** @hide */
	private final class BinderService extends IEpicManager.Stub
	{
		@Override
		public IEpicObject Create(int scenario_id)
		{
			return new EpicObject(scenario_id);
		}

		@Override
		public IEpicObject Creates(int[] scenario_id_list)
		{
			return new EpicObject(scenario_id_list);
		}
	}
}
