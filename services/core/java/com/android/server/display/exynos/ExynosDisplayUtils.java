    /*
     * Copyright (C) 2007 The Android Open Source Project
     * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *      http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

package com.android.server.display.exynos;

import android.os.RemoteException;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.ServiceManager;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import java.nio.charset.StandardCharsets;

import java.util.List;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

/**
 * Various utilities to Exynos Display Solution.
 */
final class ExynosDisplayUtils {
	private static final String TAG = "ExynosDisplayUtils";

	private ExynosDisplayUtils() { /* cannot be instantiated */ }
/**
 * The java.io.File.exists() method tests the existence of the file or directory defined by this abstract pathname.
 * The java.io.File.isFile() checks whether the file denoted by this abstract pathname is a normal file.
 */
	static String getStringFromFile(String fileName) {
		File file = new File(fileName);

		if (!file.exists()) {
			Log.e(TAG, fileName + " File not found");
			return null;
		}

		if (!file.isFile()) {
			Log.e(TAG, fileName + " is not File");
			return null;
		}

		InputStream in = null;
		final int MAX_BUFFER_SIZE = 1024;
		byte[] buffer = new byte[MAX_BUFFER_SIZE];
		String value = null;
		int length = 0;

		for (int i = 0; i < MAX_BUFFER_SIZE; i++) {
			buffer[i] = '\0';
		}

		try {
			if (fileName != null) {
				in = new FileInputStream(new File(fileName));
			}
			if (in != null) {
				length = in.read(buffer);
				if ((length) > 0) {
					value = new String(buffer, 0, length - 1, StandardCharsets.UTF_8);
				}
				in.close();
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception : " + e + " , in : " + in + " , value : " + value +" , length : " + length);
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception ee) {
					Log.e(TAG, "File Close error");
				}
			}
		}
		return value;
	}

	static boolean sysfsWrite(String sysfs, int value) {
		FileOutputStream out = null;
		File myfile = new File(sysfs);
		if (myfile.exists()) {
			try {
				try {
					out = new FileOutputStream(myfile);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					return false;
				}
				out.write(Integer.toString(value).getBytes());
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
				try {
					out.close();
				} catch (Exception err) {
					err.printStackTrace();
				}
				return false;
			}
			return true;
		}
		return false;
	}

	static boolean sysfsWriteSting(String sysfs, String value) {
		FileOutputStream out = null;
		File myfile = new File(sysfs);
		if (myfile.exists()) {
			try {
				try {
					out = new FileOutputStream(myfile);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					return false;
				}
				out.write(value.getBytes());
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
				try {
					out.close();
				} catch (Exception err) {
					err.printStackTrace();
				}
				return false;
			}
			return true;
		}
		return false;
	}

	static boolean existFile(String file_path) {
		File file = new File(file_path);

		if (!file.exists()) {
			Log.e(TAG, file_path + " File not found");
			return false;
		}

		if (!file.isFile()) {
			Log.e(TAG, file_path + " is not File");
			return false;
		}
		return true;
	}

	static String[] parserXML(String xml_path, String mode_name, String item_name) {
		FileInputStream fis = null;
		String[] return_array = null;

		Log.d(TAG, "xml_path = " + xml_path + ", mode = " + mode_name + ", item = " + item_name);

		File file = new File(xml_path);
		if (!file.isFile()) {
			Log.e(TAG, xml_path + " File not found");
			return null;
		}

		try {
			final int STEP_NONE = 0 ;
			final int STEP_MODE = 1 ;
			final int STEP_ITEM = 11;
			final int STEP_ITEM_CGC = 12;
			final int STEP_ITEM_GAMMA = 13;
			final int STEP_ITEM_HSC = 14;

			int step1 = STEP_NONE;
			int step2 = STEP_NONE;

			fis = new FileInputStream(xml_path);
			if (fis == null) {
				Log.e(TAG, xml_path + " File not found");
				return null;
			}

			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser xpp = factory.newPullParser();
			xpp.setInput(fis, null);
			int eventType = xpp.getEventType();

			List<String> input = new ArrayList<String>();

			while (eventType != xpp.END_DOCUMENT) {
				if(eventType == xpp.START_DOCUMENT) {
					/*Log.d(TAG, "Start document");*/
				} else if(eventType == xpp.END_DOCUMENT) {
					/*Log.d(TAG, "End document");*/
				} else if(eventType == xpp.START_TAG) {
					String startTag = xpp.getName();

					if (startTag.equals("mode")) {
						String attrValue = xpp.getAttributeValue(null, "name");
						if (attrValue.equals(mode_name))
							step1 = STEP_MODE;
					}

					if (item_name.equals("dqe")) {
						if (startTag.equals("cgc"))
							step2 = STEP_ITEM_CGC;
						if (startTag.equals("gamma"))
							step2 = STEP_ITEM_GAMMA;
						if (startTag.equals("hsc"))
							step2 = STEP_ITEM_HSC;
					} else {
						if (startTag.equals(item_name))
							step2 = STEP_ITEM;
					}
				} else if(eventType == xpp.END_TAG) {
					String endTag = xpp.getName();

					if (endTag.equals("mode"))
						step1 = STEP_NONE;

					if (item_name.equals("dqe")) {
						if (endTag.equals("cgc"))
							step2 = STEP_NONE;
						if (endTag.equals("gamma"))
							step2 = STEP_NONE;
						if (endTag.equals("hsc"))
							step2 = STEP_NONE;
					} else {
						if (endTag.equals(item_name))
							step2 = STEP_NONE;
					}
				} else if(eventType == xpp.TEXT) {
					if (item_name.equals("dqe")) {
						if (step1 == STEP_MODE && step2 == STEP_ITEM_CGC)
							input.add(xpp.getText());
						if (step1 == STEP_MODE && step2 == STEP_ITEM_GAMMA)
							input.add(xpp.getText());
						if (step1 == STEP_MODE && step2 == STEP_ITEM_HSC)
							input.add(xpp.getText());
					} else {
						if (step1 == STEP_MODE && step2 == STEP_ITEM)
							input.add(xpp.getText());
					}
				}
				eventType = xpp.next();
			}

			return_array = new String[input.size()];

			for (int i = 0; i < input.size(); i++) {
				return_array[i] = input.get(i);
				return_array[i] = return_array[i].trim();
			}

			/*Log.d(TAG, "input size: " + input.size());*/
			/*Log.d(TAG, "return_array size: " + return_array.length);*/
			/*for (int i = 0; i < return_array.length; i++)
				Log.d(TAG, "[" + String.format("%03d", i) + "] " + return_array[i]);*/

			return return_array;

		} catch (XmlPullParserException e) {
			/*TODO Auto-generated catch block*/
			e.printStackTrace();
		} catch (IOException e) {
			/*TODO Auto-generated catch block*/
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception ee) {
					Log.e(TAG, "File Close error");
				}
			}
		}
		return null;
	}

	static String[] parserTuneXML(String xml_path, String mode_name, String item_name) {
		FileInputStream fis = null;
		String[] return_array = null;

		/*Log.d(TAG, "xml_path = " + xml_path + ", mode = " + mode_name + ", item = " + item_name);*/

		File file = new File(xml_path);
		if (!file.isFile()) {
			Log.e(TAG, xml_path + " File not found");
			return null;
		}

		try {
			final int STEP_NONE = 0 ;
			final int STEP_MODE = 1 ;
			final int STEP_ITEM = 11;
			final int STEP_ITEM_CGC = 12;
			final int STEP_ITEM_GAMMA = 13;
			final int STEP_ITEM_HSC = 14;

			int step1 = STEP_NONE;
			int step2 = STEP_NONE;

			fis = new FileInputStream(xml_path);
			if (fis == null) {
				Log.e(TAG, xml_path + " File not found");
				return null;
			}

			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser xpp = factory.newPullParser();
			xpp.setInput(fis, null);
			int eventType = xpp.getEventType();

			List<String> input = new ArrayList<String>();
			List<String> attrEnable = new ArrayList<String>();
			String attrValue = null;

			while (eventType != xpp.END_DOCUMENT) {
				if(eventType == xpp.START_DOCUMENT) {
					/*Log.d(TAG, "Start document");*/
				} else if(eventType == xpp.END_DOCUMENT) {
					/*Log.d(TAG, "End document");*/
				} else if(eventType == xpp.START_TAG) {
					String startTag = xpp.getName();

					if (startTag.equals("mode")) {
						attrValue = xpp.getAttributeValue(null, "name");
						if (attrValue.equals(mode_name))
							step1 = STEP_MODE;
					}

					if (startTag.equals("cgc")) {
						step2 = STEP_ITEM_CGC;
						attrValue = xpp.getAttributeValue(null, "enable");
						if (attrValue != null)
							attrEnable.add(attrValue);
						else
							attrEnable.add("1");

					}
					if (startTag.equals("gamma")) {
						step2 = STEP_ITEM_GAMMA;
						attrValue = xpp.getAttributeValue(null, "enable");
						if (attrValue != null)
							attrEnable.add(attrValue);
						else
							attrEnable.add("1");
					}
					if (startTag.equals("hsc")) {
						step2 = STEP_ITEM_HSC;
						attrValue = xpp.getAttributeValue(null, "enable");
						if (attrValue != null)
							attrEnable.add(attrValue);
						else
							attrEnable.add("1");
					}

				} else if(eventType == xpp.END_TAG) {
					String endTag = xpp.getName();

					if (endTag.equals("mode"))
						step1 = STEP_NONE;

					if (endTag.equals("cgc"))
						step2 = STEP_NONE;
					if (endTag.equals("gamma"))
						step2 = STEP_NONE;
					if (endTag.equals("hsc"))
						step2 = STEP_NONE;

				} else if(eventType == xpp.TEXT) {
					if (step1 == STEP_MODE && step2 == STEP_ITEM_CGC)
						input.add(xpp.getText());
					if (step1 == STEP_MODE && step2 == STEP_ITEM_GAMMA)
						input.add(xpp.getText());
					if (step1 == STEP_MODE && step2 == STEP_ITEM_HSC)
						input.add(xpp.getText());
				}
				eventType = xpp.next();
			}

			return_array = new String[input.size() + attrEnable.size()];

			for (int i = 0; i < input.size(); i++) {
				return_array[i] = input.get(i);
				return_array[i] = return_array[i].trim();
			}

			for (int i = input.size(), j = 0; i < (input.size() + attrEnable.size()); i++, j++) {
				return_array[i] = attrEnable.get(j);
				return_array[i] = return_array[i].trim();
			}

			return return_array;

		} catch (XmlPullParserException e) {
			/*TODO Auto-generated catch block*/
			e.printStackTrace();
		} catch (IOException e) {
			/*TODO Auto-generated catch block*/
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception ee) {
					Log.e(TAG, "File Close error");
				}
			}
		}
		return null;
	}

	static void sendEmptyUpdate() {
	/*magic communication with surface flinger*/
		/*Log.d(TAG, "sendEmptyUpdate");*/
		try {
			IBinder flinger = ServiceManager.getService("SurfaceFlinger");
			if (flinger != null) {
				Parcel data = Parcel.obtain();
				data.writeInterfaceToken("android.ui.ISurfaceComposer");
				data.writeInt(0);
				flinger.transact(1006, data, null, 0); /*send empty update signalRefresh()*/
				data.recycle();
			}
		} catch (RemoteException e) {
			Log.d(TAG, "failed to sendEmptyUpdate");
		}
	}
}
