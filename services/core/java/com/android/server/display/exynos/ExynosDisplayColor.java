    /*
     * Copyright (C) 2007 The Android Open Source Project
     * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *      http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

package com.android.server.display.exynos;

import android.util.Log;

/**
 * Exynos Display Color Control.
 */
class ExynosDisplayColor {
	private static final String TAG = "ExynosDisplayColor";
	private final boolean DEBUG = "eng".equals(android.os.Build.TYPE);

	private String GAMMA_SYSFS_PATH = "/sys/class/dqe/dqe/gamma";
	private String CGC_SYSFS_PATH = "/sys/class/dqe/dqe/cgc";
	private String HSC_SYSFS_PATH = "/sys/class/dqe/dqe/hsc";

	private String XML_SYSFS_PATH = "/sys/class/dqe/dqe/xml";
	private String TUNE_MODE1_SYSFS_PATH = "/sys/class/dqe/dqe/tune_mode1";
	private String TUNE_MODE2_SYSFS_PATH = "/sys/class/dqe/dqe/tune_mode2";
	private String TUNE_MODE3_SYSFS_PATH = "/sys/class/dqe/dqe/tune_mode3";
	private String TUNE_ONOFF_SYSFS_PATH = "/sys/class/dqe/dqe/tune_onoff";
	private String COLOR_MODE_SYSFS_PATH = "/sys/class/dqe/dqe/aosp_colors";

	private String COLORTEMP_XML_FILE_PATH = "/vendor/etc/dqe/calib_data_colortemp.xml";
	private String EYETEMP_XML_FILE_PATH = "/vendor/etc/dqe/calib_data_eyetemp.xml";
	private String BYPASS_XML_FILE_PATH = "/vendor/etc/dqe/calib_data_bypass.xml";
	private String RGBGAIN_XML_FILE_PATH = "/vendor/etc/dqe/calib_data_rgbgain.xml";
	private String SKINCOLOR_XML_FILE_PATH = "/vendor/etc/dqe/calib_data_skincolor.xml";
	private String CE_XML_FILE_PATH = "/vendor/etc/dqe/calib_data_ce.xml";
	private String WHITEPOINT_XML_FILE_PATH = "/vendor/etc/dqe/calib_data_whitepoint.xml";

	private String[] colortemp_array = null;
	private String[] eyetemp_array = null;
	private String[] gamma_bypass_array = null;

	private String[] rgain_array = null;
	private String[] ggain_array = null;
	private String[] bgain_array = null;
	private String[] skincolor_array = null;
	private String[] whitepoint_array = null;
	private String[] hsc_bypass_array = null;

	private float[] rgb_gain = {1.0f, 1.0f, 1.0f};

	public ExynosDisplayColor() {

	}

	public void setColorMode(int value) {
		String xml_path = ExynosDisplayUtils.getStringFromFile(XML_SYSFS_PATH);

		if (xml_path == null)
			return;

		if (!ExynosDisplayUtils.existFile(xml_path))
			return;

		String tune_mode1 = null, tune_mode2 = null, tune_mode3 = null, tune_onoff = null;
		String temp_array[] = null;
		String color_mode = null;

		temp_array = ExynosDisplayUtils.parserXML(xml_path, "mode1", "dqe");
		if (temp_array != null && temp_array.length == 3)
			tune_mode1 = temp_array[0] + "," + temp_array[1] + "," + temp_array[2] + ",";

		temp_array = ExynosDisplayUtils.parserXML(xml_path, "mode2", "dqe");
		if (temp_array != null && temp_array.length == 3)
			tune_mode2 = temp_array[0] + "," + temp_array[1] + "," + temp_array[2] + ",";

		temp_array = ExynosDisplayUtils.parserXML(xml_path, "mode3", "dqe");
		if (temp_array != null && temp_array.length == 3)
			tune_mode3 = temp_array[0] + "," + temp_array[1] + "," + temp_array[2] + ",";

		temp_array = ExynosDisplayUtils.parserXML(xml_path, "onoff", "dqe");
		if (temp_array != null && temp_array.length == 3)
			tune_onoff = temp_array[0] + "," + temp_array[1] + "," + temp_array[2] + ",";

		if (tune_mode1 != null)
			ExynosDisplayUtils.sysfsWriteSting(TUNE_MODE1_SYSFS_PATH, tune_mode1);
		if (tune_mode2 != null)
			ExynosDisplayUtils.sysfsWriteSting(TUNE_MODE2_SYSFS_PATH, tune_mode2);
		if (tune_mode3 != null)
			ExynosDisplayUtils.sysfsWriteSting(TUNE_MODE3_SYSFS_PATH, tune_mode3);
		if (tune_onoff != null)
			ExynosDisplayUtils.sysfsWriteSting(TUNE_ONOFF_SYSFS_PATH, tune_onoff);

		try {
			color_mode = ExynosDisplayUtils.getStringFromFile(COLOR_MODE_SYSFS_PATH);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if ((!(color_mode == null)) && (!color_mode.equals("0"))) {
			Log.d(TAG, "setColorMode(): value = " + value);
			ExynosDisplayUtils.sysfsWrite(COLOR_MODE_SYSFS_PATH, value);
		}
	}

	void setColorTempValue(int value) {
		String stream = null;

		try {
			if (colortemp_array == null || colortemp_array.length == 0)
				return;
			if (value >= colortemp_array.length)
				return;

			stream = colortemp_array[value];
			/*Log.d(TAG, "gamma = " + stream);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (stream != null) {
			Log.d(TAG, "setColorTempValue()");
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, stream);
		}
	}

	void setColorTempOn(int onoff) {
		String stream = null;

		try {
			if (onoff != 0)
				colortemp_array = ExynosDisplayUtils.parserXML(COLORTEMP_XML_FILE_PATH, "colortemp", "gamma");
			else
				colortemp_array = null;

			if (gamma_bypass_array == null)
				gamma_bypass_array = ExynosDisplayUtils.parserXML(BYPASS_XML_FILE_PATH, "bypass", "gamma");

			if (gamma_bypass_array == null || gamma_bypass_array.length == 0)
				return;

			stream = gamma_bypass_array[0];
			/*Log.d(TAG, "gamma = " + stream);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (stream != null) {
			Log.d(TAG, "setColorTempOn()");
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, stream);
		}
	}

	void setEyeTempValue(int value) {
		String stream = null;

		try {
			if (eyetemp_array == null || eyetemp_array.length == 0)
				return;
			if (value >= eyetemp_array.length)
				return;

			stream = eyetemp_array[value];
			/*Log.d(TAG, "gamma = " + stream);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (stream != null) {
			Log.d(TAG, "setEyeTempValue()");
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, stream);
		}
	}

	void setEyeTempOn(int onoff) {
		String stream = null;

		try {
			if (onoff != 0)
				eyetemp_array = ExynosDisplayUtils.parserXML(EYETEMP_XML_FILE_PATH, "eyetemp", "gamma");
			else
				eyetemp_array = null;

			if (gamma_bypass_array == null)
				gamma_bypass_array = ExynosDisplayUtils.parserXML(BYPASS_XML_FILE_PATH, "bypass", "gamma");

			if (gamma_bypass_array == null || gamma_bypass_array.length == 0)
				return;

			stream = gamma_bypass_array[0];
			/*Log.d(TAG, "gamma = " + stream);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (stream != null) {
			Log.d(TAG, "setEyeTempOn()");
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, stream);
		}
	}

	void setRgbGainValue(int r, int g, int b) {
		String stream = null;

		try {
			if (rgain_array == null || rgain_array.length == 0)
				return;
			if (ggain_array == null || ggain_array.length == 0)
				return;
			if (bgain_array == null || bgain_array.length == 0)
				return;

			if (r >= rgain_array.length)
				return;
			if (g >= ggain_array.length)
				return;
			if (b >= bgain_array.length)
				return;

			stream = rgain_array[r] + "," + ggain_array[g] + "," + bgain_array[b];
			/*Log.d(TAG, "gamma = " + stream);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (stream != null) {
			Log.d(TAG, "setRgbGainValue()");
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, stream);
		}
	}

	void setRgbGainOn(int onoff) {
		String stream = null;

		try {
			if (onoff != 0) {
				rgain_array = ExynosDisplayUtils.parserXML(RGBGAIN_XML_FILE_PATH, "rgbgain", "red");
				ggain_array = ExynosDisplayUtils.parserXML(RGBGAIN_XML_FILE_PATH, "rgbgain", "green");
				bgain_array = ExynosDisplayUtils.parserXML(RGBGAIN_XML_FILE_PATH, "rgbgain", "blue");
			} else {
				rgain_array = null;
				ggain_array = null;
				bgain_array = null;
			}

			if (gamma_bypass_array == null)
				gamma_bypass_array = ExynosDisplayUtils.parserXML(BYPASS_XML_FILE_PATH, "bypass", "gamma");
			if (gamma_bypass_array == null || gamma_bypass_array.length == 0)
				return;

			stream = gamma_bypass_array[0];
			/*Log.d(TAG, "gamma = " + stream);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (stream != null) {
			Log.d(TAG, "setRgbGainOn()");
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, stream);
		}
	}

	void setSkinColorOn(int onoff) {
		String stream = null;

		try {
			if (onoff != 0)
				skincolor_array = ExynosDisplayUtils.parserXML(SKINCOLOR_XML_FILE_PATH, "skincolor", "hsc");
			else
				skincolor_array = ExynosDisplayUtils.parserXML(BYPASS_XML_FILE_PATH, "bypass", "hsc");

			if (skincolor_array == null || skincolor_array.length == 0)
				return;

			stream = skincolor_array[0];
			/*Log.d(TAG, "hsc = " + stream);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (stream != null) {
			Log.d(TAG, "setSkinColorOn()");
			ExynosDisplayUtils.sysfsWriteSting(HSC_SYSFS_PATH, stream);
		}
	}

	void setHsvGainValue(int h, int s, int v) {
		String stream = null;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			if (hsc_bypass_array == null || hsc_bypass_array.length == 0)
				return;

			String[] hsc_lut = hsc_bypass_array[0].split(",");

			hsc_lut[9] = Integer.toString(1);	/*HSC_GSC_ON*/
			hsc_lut[10] = Integer.toString(1);	/*HSC_GHC_ON*/
			hsc_lut[11] = Integer.toString(1);	/*HSC_GBC_ON*/
			hsc_lut[12] = Integer.toString(s - 127); /*HSC_GSC_GAIN*/
			hsc_lut[13] = Integer.toString(h - 127); /*HSC_GHC_GAIN*/
			hsc_lut[14] = Integer.toString(v - 127); /*HSC_GBC_GAIN*/

			hsc_lut[146] = Integer.toString(255);	/*POLY_CURVE1*/
			hsc_lut[147] = Integer.toString(255);	/*POLY_CURVE2*/
			hsc_lut[148] = Integer.toString(255);	/*POLY_CURVE3*/
			hsc_lut[149] = Integer.toString(255);	/*POLY_CURVE4*/
			hsc_lut[150] = Integer.toString(255);	/*POLY_CURVE5*/
			hsc_lut[151] = Integer.toString(255);	/*POLY_CURVE6*/
			hsc_lut[152] = Integer.toString(255);	/*POLY_CURVE7*/
			hsc_lut[153] = Integer.toString(255);	/*POLY_CURVE8*/

			for (int i = 0; i < hsc_lut.length; i++) {
				String temp_hsc =  (i < (hsc_lut.length -1)) ? (hsc_lut[i] + ",") : (hsc_lut[i]);
				stringBuilder.append(temp_hsc);
			}

			if (stringBuilder.length() > 0)
				stream = stringBuilder.toString();
			/*Log.d(TAG, "hsc = " + stream);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (stream != null) {
			Log.d(TAG, "setHsvGainValue()");
			ExynosDisplayUtils.sysfsWriteSting(HSC_SYSFS_PATH, stream);
		}
	}

	void setHsvGainValueOldVer(int h, int s, int v) {
		String stream = null;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			if (hsc_bypass_array == null || hsc_bypass_array.length == 0)
				return;

			String[] hsc_lut = hsc_bypass_array[0].split(",");

			if (false) {
				hsc_lut[0] = Integer.toString(1);	/*PPSC_ON*/
				hsc_lut[1] = Integer.toString(s - 127); /*PPSCGAIN_R*/
				hsc_lut[2] = Integer.toString(s - 127); /*PPSCGAIN_G*/
				hsc_lut[3] = Integer.toString(s - 127); /*PPSCGAIN_B*/
				hsc_lut[4] = Integer.toString(s - 127); /*PPSCGAIN_C*/
				hsc_lut[5] = Integer.toString(s - 127); /*PPSCGAIN_M*/
				hsc_lut[6] = Integer.toString(s - 127); /*PPSCGAIN_Y*/
			}

			hsc_lut[7] = Integer.toString(0);	/*YCOMP_ON*/

			hsc_lut[9] = Integer.toString(1);	/*TSC_ON*/
			hsc_lut[10] = Integer.toString(s - 127); /*TSCGAIN*/

			hsc_lut[12] = Integer.toString(1);	/*PPHC_ON*/
			hsc_lut[13] = Integer.toString(h - 127); /*PPHCGAIN_R*/
			hsc_lut[14] = Integer.toString(h - 127); /*PPHCGAIN_G*/
			hsc_lut[15] = Integer.toString(h - 127); /*PPHCGAIN_B*/
			hsc_lut[16] = Integer.toString(h - 127); /*PPHCGAIN_C*/
			hsc_lut[17] = Integer.toString(h - 127); /*PPHCGAIN_M*/
			hsc_lut[18] = Integer.toString(h - 127); /*PPHCGAIN_Y*/

			hsc_lut[22] = Integer.toString(255);	/*POLY_CURVE1*/
			hsc_lut[23] = Integer.toString(255);	/*POLY_CURVE2*/
			hsc_lut[24] = Integer.toString(255);	/*POLY_CURVE3*/
			hsc_lut[25] = Integer.toString(255);	/*POLY_CURVE4*/
			hsc_lut[26] = Integer.toString(255);	/*POLY_CURVE5*/
			hsc_lut[27] = Integer.toString(255);	/*POLY_CURVE6*/
			hsc_lut[28] = Integer.toString(255);	/*POLY_CURVE7*/
			hsc_lut[29] = Integer.toString(255);	/*POLY_CURVE8*/

			hsc_lut[30] = Integer.toString(0);	/*SKIN_ON*/

			for (int i = 0; i < hsc_lut.length; i++) {
				String temp_hsc =  (i < (hsc_lut.length -1)) ? (hsc_lut[i] + ",") : (hsc_lut[i]);
				stringBuilder.append(temp_hsc);
			}

			if (stringBuilder.length() > 0)
				stream = stringBuilder.toString();
			/*Log.d(TAG, "hsc = " + stream);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (stream != null) {
			Log.d(TAG, "setHsvGainValue()");
			ExynosDisplayUtils.sysfsWriteSting(HSC_SYSFS_PATH, stream);
		}
	}

	void setHsvGainOn(int onoff) {
		String stream = null;

		try {
			if (hsc_bypass_array == null)
				hsc_bypass_array = ExynosDisplayUtils.parserXML(BYPASS_XML_FILE_PATH, "bypass", "hsc");

			if (hsc_bypass_array == null || hsc_bypass_array.length == 0)
				return;

			stream = hsc_bypass_array[0];
			/*Log.d(TAG, "hsc = " + stream);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (stream != null) {
			Log.d(TAG, "setHsvGainOn()");
			ExynosDisplayUtils.sysfsWriteSting(HSC_SYSFS_PATH, stream);
		}
	}

	String getColorEnhancementMode() {
		String mode_string = "Off,UI,Gallery,Browser,Camera,Video";
		return mode_string;
	}

	void setColorEnhancement(int value) {
		if (!ExynosDisplayUtils.existFile(CE_XML_FILE_PATH))
			return;

		String cgc_stream = null, gamma_stream = null, hsc_stream = null;
		String temp_array[] = null;

		try {
			switch (value) {
			case 0:
				temp_array = ExynosDisplayUtils.parserXML(CE_XML_FILE_PATH, "ce_off", "dqe");
				break;
			case 1:
				temp_array = ExynosDisplayUtils.parserXML(CE_XML_FILE_PATH, "ce_ui", "dqe");
				break;
			case 2:
				temp_array = ExynosDisplayUtils.parserXML(CE_XML_FILE_PATH, "ce_gallery", "dqe");
				break;
			case 3:
				temp_array = ExynosDisplayUtils.parserXML(CE_XML_FILE_PATH, "ce_browser", "dqe");
				break;
			case 4:
				temp_array = ExynosDisplayUtils.parserXML(CE_XML_FILE_PATH, "ce_camera", "dqe");
				break;
			case 5:
				temp_array = ExynosDisplayUtils.parserXML(CE_XML_FILE_PATH, "ce_video", "dqe");
				break;
			default:
				break;
			}

			if (temp_array != null && temp_array.length == 3) {
				cgc_stream = temp_array[0];
				gamma_stream = temp_array[1];
				hsc_stream = temp_array[2];
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (cgc_stream != null)
			ExynosDisplayUtils.sysfsWriteSting(CGC_SYSFS_PATH, cgc_stream);

		if (gamma_stream != null)
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, gamma_stream);

		if (hsc_stream != null)
			ExynosDisplayUtils.sysfsWriteSting(HSC_SYSFS_PATH, hsc_stream);
	}

	void setWhitePointColorOn(int onoff) {
		String stream = null;

		try {
			if (onoff != 0)
				whitepoint_array = ExynosDisplayUtils.parserXML(WHITEPOINT_XML_FILE_PATH, "whitepoint", "cgc");
			else
				whitepoint_array = ExynosDisplayUtils.parserXML(BYPASS_XML_FILE_PATH, "bypass", "cgc");

			if (whitepoint_array == null || whitepoint_array.length == 0)
				return;

			stream = whitepoint_array[0];
			/*Log.d(TAG, "cgc = " + stream);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (stream != null) {
			Log.d(TAG, "setWhitePointColorOn()");
			ExynosDisplayUtils.sysfsWriteSting(CGC_SYSFS_PATH, stream);
		}
	}

	void setRgbGain(float r, float g, float b) {
		String stream = null;

		rgb_gain[0] = r;
		rgb_gain[1] = g;
		rgb_gain[2] = b;

		try {
			int[] gc = new int[65 * 3];
			String[] gc_lut = new String[65 * 3];
			StringBuilder stringBuilder = new StringBuilder();

			for (int i = 0; i < 65; i++) {
				gc[i] = (int)Math.round((double)(r * 255.0f) / 64 * i);
				gc[i+65] = (int)Math.round((double)(g * 255.0f) / 64 * i);
				gc[i+130] = (int)Math.round((double)(b * 255.0f) / 64 * i);
			}

			for (int i = 0; i < (65 * 3); i++)
				gc_lut[i] = Integer.toString(gc[i]);

			for (int i = 0; i < gc_lut.length; i++) {
				String temp_gc =  (i < (gc_lut.length -1)) ? (gc_lut[i] + ",") : (gc_lut[i]);
				stringBuilder.append(temp_gc);
			}

			if (stringBuilder.length() > 0)
				stream = stringBuilder.toString();

			/*Log.d(TAG, "gamma = " + stream);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (stream != null) {
			Log.d(TAG, "setRgbGain(): r=" + (r * 255.0f) + ", g=" + (g * 255.0f) + ", b=" + (b * 255.0f));
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, stream);
		}
	}

	float[] getRgbGain() {
		return rgb_gain;
	}
}
