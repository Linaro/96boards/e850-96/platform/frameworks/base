    /*
     * Copyright (C) 2007 The Android Open Source Project
     * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *      http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

package com.android.server.display.exynos;

import java.io.File;
import android.util.Log;

/**
 * Exynos Display Panel Control.
 */
class ExynosDisplayPanel {
	private static final String TAG = "ExynosDisplayPanel";
	private final boolean DEBUG = "eng".equals(android.os.Build.TYPE);

	private String PANEL_CABC_MODE_PATH = "/sys/class/panel/panel/cabc_mode";
	private String PANEL0_CABC_MODE_PATH = "/sys/devices/platform/panel_0/cabc_mode";
	private String CABC_MODE_VALUE = null;

	private String PANEL0_HBM_MODE_PATH = "/sys/devices/platform/panel_0/hbm_mode";
	private String HBM_MODE_VALUE = null;

	public ExynosDisplayPanel() {

	}

	private boolean existPanelFile(String file_path) {
		File file = new File(file_path);

		if (!file.exists()) {
			/*Log.e(TAG, file_path + " File not found");*/
			return false;
		}

		if (!file.isFile()) {
			/*Log.e(TAG, file_path + " is not File");*/
			return false;
		}
		return true;
	}

	public void setCABCMode(int value) {
		String cabc_path = null;

		try {
			if (existPanelFile(PANEL_CABC_MODE_PATH)) {
				CABC_MODE_VALUE = ExynosDisplayUtils.getStringFromFile(PANEL_CABC_MODE_PATH);
				cabc_path = PANEL_CABC_MODE_PATH;
			} else if (existPanelFile(PANEL0_CABC_MODE_PATH)) {
				CABC_MODE_VALUE = ExynosDisplayUtils.getStringFromFile(PANEL0_CABC_MODE_PATH);
				cabc_path = PANEL0_CABC_MODE_PATH;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if ((!(CABC_MODE_VALUE == null)) && (!CABC_MODE_VALUE.equals("0"))) {
			Log.d(TAG, "setCABCMode(): value = " + value);
			ExynosDisplayUtils.sysfsWrite(cabc_path, value);
		}
	}

	public void setHBMMode(int value) {
		try {
			HBM_MODE_VALUE = ExynosDisplayUtils.getStringFromFile(PANEL0_HBM_MODE_PATH);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if ((!(HBM_MODE_VALUE == null)) && (!HBM_MODE_VALUE.equals("0"))) {
			Log.d(TAG, "setHBMMode(): value = " + value);
			ExynosDisplayUtils.sysfsWrite(PANEL0_HBM_MODE_PATH, value);
		}
	}
}
