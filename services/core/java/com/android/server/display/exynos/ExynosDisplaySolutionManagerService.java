    /*
     * Copyright (C) 2007 The Android Open Source Project
     * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *      http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

package com.android.server.display.exynos;

import android.os.Parcel;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.Looper;
import android.os.Message;
import android.os.UserHandle;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.RemoteException;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import android.hardware.display.ExynosDisplaySolutionManager;
import android.hardware.display.IExynosDisplaySolutionManager;
import com.android.server.display.exynos.ExynosDisplaySolution;

/** @hide */
public class ExynosDisplaySolutionManagerService extends IExynosDisplaySolutionManager.Stub{
	private static final String TAG = "ExynosDisplaySolutionManagerService";
	private final boolean DEBUG = "eng".equals(android.os.Build.TYPE);

	private final Object mLock = new Object();
	private final Context mContext;
	private String mColorModeName;

	private ExynosDisplaySolution mExynosDisplay = null;
	private ExynosDisplayTune mExynosDisplayTune = null;
	private ExynosDisplayPanel mExynosDisplayPanel = null;
	private ExynosDisplayColor mExynosDisplayColor = null;

	public ExynosDisplaySolutionManagerService(Context context) {
		mContext = context;
		mExynosDisplay = new ExynosDisplaySolution(context);
		mExynosDisplayTune = new ExynosDisplayTune();
		mExynosDisplayPanel = new ExynosDisplayPanel();
		mExynosDisplayColor = new ExynosDisplayColor();
	}

	@Override // Binder call
	public void setCABCModeSettingValue(int value) {
		synchronized (mLock) {
			Log.d(TAG, "setCABCModeSettingValue(): value = " + value);
			mExynosDisplayPanel.setCABCMode(value);
		}
	}

	@Override // Binder call
	public String getColorEnhancementMode() {
		synchronized (mLock) {
			mColorModeName = mExynosDisplayColor.getColorEnhancementMode();
			Log.d(TAG, "getColorEnhancementMode(): mColorModeName = " + mColorModeName);
			return mColorModeName;
		}
	}

	@Override // Binder call
	public void setColorEnhancementSettingValue(int value) {
		synchronized (mLock) {
			Log.d(TAG, "setColorEnhancementSettingValue(): value = " + value);
			mExynosDisplayColor.setColorEnhancement(value);
		}
	}

	@Override // Binder call
	public void setColorTempSettingValue(int value) {
		synchronized (mLock) {
			Log.d(TAG, "setColorTempSettingValue(): value = " + value);
			mExynosDisplayColor.setColorTempValue(value);
		}
	}

	@Override // Binder call
	public void setColorTempSettingOn(int onoff) {
		synchronized (mLock) {
			Log.d(TAG, "setColorTempSettingOn(): onoff = " + onoff);
			mExynosDisplayColor.setColorTempOn(onoff);
		}
	}

	@Override // Binder call
	public void setEyeTempSettingValue(int value) {
		synchronized (mLock) {
			Log.d(TAG, "setEyeTempSettingValue(): value = " + value);
			mExynosDisplayColor.setEyeTempValue(value);
		}
	}

	@Override // Binder call
	public void setEyeTempSettingOn(int onoff) {
		synchronized (mLock) {
			Log.d(TAG, "setEyeTempSettingOn(): onoff = " + onoff);
			mExynosDisplayColor.setEyeTempOn(onoff);
		}
	}

	@Override // Binder call
	public void setRgbGainSettingValue(int r, int g, int b) {
		synchronized (mLock) {
			Log.d(TAG, "setRgbGainSettingValue(): r=" + r + ", g=" + g + ", b=" + b);
			mExynosDisplayColor.setRgbGainValue(r, g, b );
		}
	}

	@Override // Binder call
	public void setRgbGainSettingOn(int onoff) {
		synchronized (mLock) {
			Log.d(TAG, "setRgbGainSettingOn(): onoff = " + onoff);
			mExynosDisplayColor.setRgbGainOn(onoff);
		}
	}

	@Override // Binder call
	public void setSkinColorSettingOn(int onoff) {
		synchronized (mLock) {
			Log.d(TAG, "setSkinColorSettingOn(): onoff = " + onoff);
			mExynosDisplayColor.setSkinColorOn(onoff);
		}
	}

	@Override // Binder call
	public void setHsvGainSettingValue(int h, int s, int v) {
		synchronized (mLock) {
			Log.d(TAG, "setHsvGainSettingValue(): h=" + h + ", s=" + s + ", v=" + v);
			mExynosDisplayColor.setHsvGainValue(h, s, v );
		}
	}

	@Override // Binder call
	public void setHsvGainSettingOn(int onoff) {
		synchronized (mLock) {
			Log.d(TAG, "setHsvGainSettingOn(): onoff = " + onoff);
			mExynosDisplayColor.setHsvGainOn(onoff);
		}
	}

	@Override // Binder call
	public void setWhitePointColorSettingOn(int onoff) {
		synchronized (mLock) {
			Log.d(TAG, "setWhitePointColorSettingOn(): onoff = " + onoff);
			mExynosDisplayColor.setWhitePointColorOn(onoff);
		}
	}

	@Override // Binder call
	public void setRgbGain(float r, float g, float b) {
		synchronized (mLock) {
			Log.d(TAG, "setRgbGain(): r=" + r + ", g=" + g + ", b=" + b);
			mExynosDisplayColor.setRgbGain(r, g, b );
		}
	}

	@Override // Binder call
	public float[] getRgbGain() {
		synchronized (mLock) {
			float[] value = mExynosDisplayColor.getRgbGain();
			Log.d(TAG, "getRgbGain(): r=" + value[0] + ", g=" + value[1] + ", b=" + value[2]);
			return value;
		}
	}

       @Override // Binder call
       public void setHBMModeValue(int value) {
                synchronized (mLock) {
                       Log.d(TAG, "setHBMModeValue(): value = " + value);
                       mExynosDisplayPanel.setHBMMode(value);
                }
        }
}
