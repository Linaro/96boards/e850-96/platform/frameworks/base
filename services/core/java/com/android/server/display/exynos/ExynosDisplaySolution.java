    /*
     * Copyright (C) 2007 The Android Open Source Project
     * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *      http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

package com.android.server.display.exynos;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.UserHandle;
import android.content.Context;
import android.util.Log;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.net.Uri;

/**
 * Exynos Display Solution.
 */
public class ExynosDisplaySolution {
	private static final String TAG = "ExynosDisplaySolution";
	private final boolean DEBUG = "eng".equals(android.os.Build.TYPE);
	private final Context mContext;

	private ExynosDisplayPanel mExynosDisplayPanel = null;
	private ExynosDisplayColor mExynosDisplayColor = null;
	private ExynosDisplayATC mExynosDisplayATC = null;
	private ExynosDisplayTune mExynosDisplayTune = null;

	private ScreenBroadcastReceiver mScreenWatchingReceiver;
	private SettingsObserver mSettingsObserver;
	private boolean mAtcEnableSetting = false;
	private boolean mLightSensorEnabled = false;
	private boolean mBootCompleted = false;
	private boolean mTuneEnableSetting = false;

	public ExynosDisplaySolution(Context context) {
		mContext = context;
		mExynosDisplayATC = new ExynosDisplayATC(context);
		mExynosDisplayTune = new ExynosDisplayTune();
		mSettingsObserver = new SettingsObserver(context.getMainThreadHandler());

		final ContentResolver resolver = mContext.getContentResolver();
		resolver.registerContentObserver(
			Settings.System.getUriFor(Settings.System.ATC_MODE_ENABLED), false, mSettingsObserver, UserHandle.USER_ALL);
		resolver.registerContentObserver(
			Settings.System.getUriFor(Settings.System.DQE_TUNE_ENABLED), false, mSettingsObserver, UserHandle.USER_ALL);

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(Intent.ACTION_BOOT_COMPLETED);
		intentFilter.addAction(Intent.ACTION_SCREEN_ON);
		intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
		intentFilter.addAction(Intent.ACTION_USER_PRESENT);
		mContext.registerReceiver(new ScreenBroadcastReceiver(), intentFilter);

		settingChanged();

		Log.d(TAG, "ExynosDisplaySolution constructor");
	}

	public ExynosDisplaySolution(Context context, String name) {
		mContext = context;
		mExynosDisplayPanel = new ExynosDisplayPanel();
		mExynosDisplayColor = new ExynosDisplayColor();
	}

	public void setCABCMode(int value) {
		mExynosDisplayPanel.setCABCMode(value);
	}

	public void setColorMode(int value) {
		mExynosDisplayColor.setColorMode(value);
	}

	private final class ScreenBroadcastReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
				/*Log.d(TAG, "ACTION_BOOT_COMPLETED");*/
				mBootCompleted = true;

				if (mAtcEnableSetting)
					mExynosDisplayATC.enableLightSensor(true);

				if (mTuneEnableSetting)
					mExynosDisplayTune.enableTuneTimer(true);

			} else if (Intent.ACTION_SCREEN_ON.equals(action)) {
				/*Log.d(TAG, "ACTION_SCREEN_ON");*/

				if (mAtcEnableSetting)
					mExynosDisplayATC.enableLightSensor(true);

			} else if (Intent.ACTION_SCREEN_OFF.equals(action)) {
				/*Log.d(TAG, "ACTION_SCREEN_OFF");*/

				mExynosDisplayATC.enableLightSensor(false);

			} else if (Intent.ACTION_USER_PRESENT.equals(action)) {
				/*Log.d(TAG, "ACTION_USER_PRESENT");*/
			}
		}
	}

	private void settingChanged() {
		final ContentResolver resolver = mContext.getContentResolver();

		boolean atcEnableSetting = (Settings.System.getIntForUser(resolver,
		Settings.System.ATC_MODE_ENABLED, 0, UserHandle.USER_CURRENT) != 0);

		boolean tuneEnableSetting = (Settings.System.getIntForUser(resolver,
		Settings.System.DQE_TUNE_ENABLED, 0, UserHandle.USER_CURRENT) != 0);

		/*Log.d(TAG, "settingChanged: mAtcEnableSetting=" + mAtcEnableSetting);*/
		/*Log.d(TAG, "settingChanged: mTuneEnableSetting =" + mTuneEnableSetting);*/

		if (mAtcEnableSetting != atcEnableSetting) {
			if (mBootCompleted)
				mExynosDisplayATC.enableLightSensor(atcEnableSetting);

			if (mBootCompleted && !atcEnableSetting)
				mExynosDisplayATC.sysfsWriteOnOff(false);
		}

		if (mTuneEnableSetting != tuneEnableSetting) {
			if (mBootCompleted)
				mExynosDisplayTune.enableTuneTimer(tuneEnableSetting);
		}

		mAtcEnableSetting = atcEnableSetting;
		mTuneEnableSetting = tuneEnableSetting;
	}

	private final class SettingsObserver extends ContentObserver {
		public SettingsObserver(Handler handler) {
			super(handler);
		}

		@Override
		public void onChange(boolean selfChange, Uri uri) {
			settingChanged();
		}
	}
}
