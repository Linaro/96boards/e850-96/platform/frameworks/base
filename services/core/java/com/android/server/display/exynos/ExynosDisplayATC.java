    /*
     * Copyright (C) 2007 The Android Open Source Project
     * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *      http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

package com.android.server.display.exynos;

import android.os.Message;
import android.os.Handler;
import android.content.Context;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import android.util.Log;

/**
 * Exynos Display Adaptive Tone Control.
 */
class ExynosDisplayATC {
	private static final String TAG = "ExynosDisplayATC";
	private final boolean DEBUG = "eng".equals(android.os.Build.TYPE);

	private static final int MSG_LIGHT_SENSOR_LUX = 1;

	private static final int DELAY_LIGHT_SENSOR = 1 * 500;

	private Sensor mLightSensor = null;
	private SensorManager mSensorManager = null;
	private boolean mLightSensorEnabled = false;

	private String ATC_LUX_SYSFS_PATH = "/sys/class/dqe/dqe/aps_lux";
	private String ATC_ONOFF_SYSFS_PATH = "/sys/class/dqe/dqe/aps_onoff";

	protected ExynosDisplayATC(Context context) {
		mSensorManager = context.getSystemService(SensorManager.class);
		mLightSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
	}

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_LIGHT_SENSOR_LUX:
				sysfsWriteLux(msg.arg1);
				break;
			default :
				break;
			}
		}
	};

	private SensorEventListener sensorListener = new SensorEventListener() {
		@Override
		public void onSensorChanged(SensorEvent event) {
			final int lux = (int)event.values[0];
			/*Log.d(TAG, "lux : " + lux);*/

			if (mHandler != null) {
				Message msg = mHandler.obtainMessage();
				msg.what = MSG_LIGHT_SENSOR_LUX;
				msg.arg1 = lux;
				/*mHandler.sendMessage(msg);*/
				mHandler.sendMessageDelayed(msg, DELAY_LIGHT_SENSOR);
			}
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
	};

	protected void enableLightSensor(boolean enable) {
		if (enable) {
			Log.d(TAG, "enableLightSensor: enable=" + enable);

			if (mHandler != null) {
				mHandler.removeMessages(MSG_LIGHT_SENSOR_LUX);
			}

			if (mSensorManager != null) {
				mSensorManager.registerListener(sensorListener, mLightSensor,
					SensorManager.SENSOR_DELAY_NORMAL);
			}
		} else {
			if (mSensorManager != null) {
				mSensorManager.unregisterListener(sensorListener);
			}

			if (mHandler != null) {
				mHandler.removeMessages(MSG_LIGHT_SENSOR_LUX);
			}
		}
		mLightSensorEnabled = enable;
	}

	protected void sysfsWriteLux(int lux) {
		if (!mLightSensorEnabled)
			return;

		String atc_lux = ExynosDisplayUtils.getStringFromFile(ATC_LUX_SYSFS_PATH);

		if ((!(atc_lux == null)) && (!atc_lux.equals("-1"))) {
			Log.d(TAG, "lux : " + lux);
			ExynosDisplayUtils.sysfsWrite(ATC_LUX_SYSFS_PATH, lux);
		}
	}

	protected void sysfsWriteOnOff(boolean onoff) {
		String atc_onoff = ExynosDisplayUtils.getStringFromFile(ATC_ONOFF_SYSFS_PATH);

		if ((!(atc_onoff == null)) && (!atc_onoff.equals("0"))) {
			Log.d(TAG, "onoff : " + onoff);
			ExynosDisplayUtils.sysfsWrite(ATC_ONOFF_SYSFS_PATH, ((!onoff) ? 0 : 1));
		}
	}
}
