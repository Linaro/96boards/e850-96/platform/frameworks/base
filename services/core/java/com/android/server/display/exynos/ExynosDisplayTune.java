    /*
     * Copyright (C) 2007 The Android Open Source Project
     * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *      http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

package com.android.server.display.exynos;

import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Exynos Display Tune Mode.
 */
class ExynosDisplayTune {
	private static final String TAG = "ExynosDisplayTune";
	private final boolean DEBUG = "eng".equals(android.os.Build.TYPE);

	private String GAMMA_DIR_PATH = "/data/dqe/gamma/data/";
	private String GAMMA_RESET_PATH = "/data/dqe/gamma/data/reset";
	private String GAMMA_SYSFS_PATH = "/sys/class/dqe/dqe/gamma";

	private String CGC_DIR_PATH = "/data/dqe/cgc/data/";
	private String CGC_RESET_PATH = "/data/dqe/cgc/data/reset";
	private String CGC_SYSFS_PATH = "/sys/class/dqe/dqe/cgc";

	private String HSC_DIR_PATH = "/data/dqe/hsc/data/";
	private String HSC_RESET_PATH = "/data/dqe/hsc/data/reset";
	private String HSC_SYSFS_PATH = "/sys/class/dqe/dqe/hsc";

	private String ATC_DIR_PATH = "/data/dqe/aps/data/";
	private String ATC_RESET_PATH = "/data/dqe/aps/data/reset";
	private String ATC_ONOFF_SYSFS_PATH = "/sys/class/dqe/dqe/aps_onoff";
	private String ATC_SYSFS_PATH = "/sys/class/dqe/dqe/aps";

	private String GAMMA_ON_FILE_PATH = "/data/dqe/gamma_on";
	private String GAMMA_OFF_FILE_PATH = "/data/dqe/gamma_off";
	private String CGC_ON_FILE_PATH = "/data/dqe/cgc_on";
	private String CGC_OFF_FILE_PATH = "/data/dqe/cgc_off";
	private String HSC_ON_FILE_PATH = "/data/dqe/hsc_on";
	private String HSC_OFF_FILE_PATH = "/data/dqe/hsc_off";
	private String ATC_ON_FILE_PATH = "/data/dqe/aps_on";
	private String ATC_OFF_FILE_PATH = "/data/dqe/aps_off";

	private String MODE_NAME_FILE_PATH = "/data/dqe/mode_name.txt";
	private String MODE_NAME_VALUE = null;
	private String MODE_NAME_STREAM = null;

	private String MODE0_CGC_FILE_PATH = "/data/dqe/mode0_cgc";
	private String MODE0_GAMMA_FILE_PATH = "/data/dqe/mode0_gamma";
	private String MODE0_HSC_FILE_PATH = "/data/dqe/mode0_hsc";
	private String MODE1_CGC_FILE_PATH = "/data/dqe/mode1_cgc";
	private String MODE1_GAMMA_FILE_PATH = "/data/dqe/mode1_gamma";
	private String MODE1_HSC_FILE_PATH = "/data/dqe/mode1_hsc";
	private String MODE2_CGC_FILE_PATH = "/data/dqe/mode2_cgc";
	private String MODE2_GAMMA_FILE_PATH = "/data/dqe/mode2_gamma";
	private String MODE2_HSC_FILE_PATH = "/data/dqe/mode2_hsc";

	private String GAMMA_FILE_PATH = null;
	private String GAMMA_VALUE = null;
	private String GAMMA_STREAM = null;
	private String CGC_FILE_PATH = null;
	private String CGC_VALUE = null;
	private String CGC_STREAM = null;
	private String HSC_FILE_PATH = null;
	private String HSC_VALUE = null;
	private String HSC_STREAM = null;
	private String ATC_FILE_PATH = null;
	private String ATC_VALUE = null;
	private String ATC_ONOFF_VALUE = null;
	private String ATC_STREAM = null;

	private Timer mTuneTimer;
	private long mDelayMs = 1 * 1000;
	private long mPeriodMs = 1 * 1000;  /*1 sec*/

	private String CALIB_DATA_XML_PATH = "/data/dqe/calib_data.xml";
	private String BYPASS_XML_FILE_PATH = "/vendor/etc/dqe/calib_data_bypass.xml";
	private String[] bypass_array = null;

	public ExynosDisplayTune() {

	}

	public void setGammaValue(int value) {
		try {
			GAMMA_FILE_PATH = GAMMA_DIR_PATH.concat(Integer.toString(value));
			Log.d(TAG, "GAMMA_FILE_PATH = " + GAMMA_FILE_PATH);

			GAMMA_VALUE = ExynosDisplayUtils.getStringFromFile(GAMMA_FILE_PATH);
			/*Log.d(TAG, "GAMMA_VALUE = " + GAMMA_VALUE);*/

			/*String gamma[] = GAMMA_VALUE.trim().split("\r\n");
			int k = 0;
			for (int i = 0;  i < 3; i++) {
				for (int j = 0; j < 65; j++) {
					Log.d(TAG, "gamma[" + k + "] = " + gamma[k++]);
				}
			}*/

			GAMMA_STREAM = GAMMA_VALUE.trim().replaceAll("\r\n", ",");
			Log.d(TAG, "GAMMA_STREAM = " + GAMMA_STREAM);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (GAMMA_STREAM != null) {
			Log.d(TAG, "setGammaValue()");
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, GAMMA_STREAM);
		}

	}

	public void setGammaReset() {
		try {
			GAMMA_VALUE = ExynosDisplayUtils.getStringFromFile(GAMMA_RESET_PATH);
			Log.d(TAG, "GAMMA_RESET_PATH = " + GAMMA_RESET_PATH);

			GAMMA_STREAM = GAMMA_VALUE.trim().replaceAll("\r\n", ",");
			Log.d(TAG, "gammaStream = " + GAMMA_STREAM);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (GAMMA_STREAM != null) {
			Log.d(TAG, "setGammaReset()");
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, GAMMA_STREAM);
		}
	}

	public void setGammaOn(int onoff) {
		try {
			if (onoff != 0) {
				GAMMA_VALUE = ExynosDisplayUtils.getStringFromFile(GAMMA_ON_FILE_PATH);
				Log.d(TAG, "GAMMA_ON_FILE_PATH = " + GAMMA_ON_FILE_PATH);
			} else {
				GAMMA_VALUE = ExynosDisplayUtils.getStringFromFile(GAMMA_OFF_FILE_PATH);
				Log.d(TAG, "GAMMA_OFF_FILE_PATH = " + GAMMA_OFF_FILE_PATH);
			}

			GAMMA_STREAM = GAMMA_VALUE.trim().replaceAll("\r\n", ",");
			/*Log.d(TAG, "GAMMA_STREAM = " + GAMMA_STREAM);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (GAMMA_STREAM != null) {
			Log.d(TAG, "setGammaOn()");
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, GAMMA_STREAM);
		}

	}

	public void setCgcValue(int value) {
		try {
			CGC_FILE_PATH = CGC_DIR_PATH.concat(Integer.toString(value));
			Log.d(TAG, "CGC_FILE_PATH = " + CGC_FILE_PATH);

			CGC_VALUE = ExynosDisplayUtils.getStringFromFile(CGC_FILE_PATH);

			CGC_STREAM = CGC_VALUE.trim().replaceAll("\r\n", ",");
			Log.d(TAG, "CGC_STREAM = " + CGC_STREAM);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (CGC_STREAM != null) {
			Log.d(TAG, "setCgcValue()");
			ExynosDisplayUtils.sysfsWriteSting(CGC_SYSFS_PATH, CGC_STREAM);
		}
	}

	public void setCgcReset() {
		try {
			CGC_VALUE = ExynosDisplayUtils.getStringFromFile(CGC_RESET_PATH);
			Log.d(TAG, "CGC_RESET_PATH = " + CGC_RESET_PATH);

			CGC_STREAM = CGC_VALUE.trim().replaceAll("\r\n", ",");
			Log.d(TAG, "cgcStream = " + CGC_STREAM);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (CGC_STREAM != null) {
			Log.d(TAG, "setCgcReset()");
			ExynosDisplayUtils.sysfsWriteSting(CGC_SYSFS_PATH, CGC_STREAM);
		}
	}

	public void setCgcOn(int onoff) {
		try {
			if (onoff != 0) {
				CGC_VALUE = ExynosDisplayUtils.getStringFromFile(CGC_ON_FILE_PATH);
				Log.d(TAG, "CGC_ON_FILE_PATH = " + CGC_ON_FILE_PATH);
			} else {
				CGC_VALUE = ExynosDisplayUtils.getStringFromFile(CGC_OFF_FILE_PATH);
				Log.d(TAG, "CGC_OFF_FILE_PATH = " + CGC_OFF_FILE_PATH);
			}

			CGC_STREAM = CGC_VALUE.trim().replaceAll("\r\n", ",");
			/*Log.d(TAG, "CGC_STREAM = " + CGC_STREAM);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (CGC_STREAM != null) {
			Log.d(TAG, "setCgcOn()");
			ExynosDisplayUtils.sysfsWriteSting(CGC_SYSFS_PATH, CGC_STREAM);
		}
	}

	public void setHscValue(int value) {
		try {
			HSC_FILE_PATH = HSC_DIR_PATH.concat(Integer.toString(value));
			Log.d(TAG, "HSC_FILE_PATH = " + HSC_FILE_PATH);

			HSC_VALUE = ExynosDisplayUtils.getStringFromFile(HSC_FILE_PATH);

			HSC_STREAM = HSC_VALUE.trim().replaceAll("\r\n", ",");
			Log.d(TAG, "HSC_STREAM = " + HSC_STREAM);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (HSC_STREAM != null) {
			Log.d(TAG, "setHscValue()");
			ExynosDisplayUtils.sysfsWriteSting(HSC_SYSFS_PATH, HSC_STREAM);
		}
	}

	public void setHscReset() {
		try {
			HSC_VALUE = ExynosDisplayUtils.getStringFromFile(HSC_RESET_PATH);
			Log.d(TAG, "HSC_RESET_PATH = " + HSC_RESET_PATH);

			HSC_STREAM = HSC_VALUE.trim().replaceAll("\r\n", ",");
			Log.d(TAG, "hscStream = " + HSC_STREAM);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (HSC_STREAM != null) {
			Log.d(TAG, "setHscReset()");
			ExynosDisplayUtils.sysfsWriteSting(HSC_SYSFS_PATH, HSC_STREAM);
		}
	}

	public void setHscOn(int onoff) {
		try {
			if (onoff != 0) {
				HSC_VALUE = ExynosDisplayUtils.getStringFromFile(HSC_ON_FILE_PATH);
				Log.d(TAG, "HSC_ON_FILE_PATH = " + HSC_ON_FILE_PATH);
			} else {
				HSC_VALUE = ExynosDisplayUtils.getStringFromFile(HSC_OFF_FILE_PATH);
				Log.d(TAG, "HSC_OFF_FILE_PATH = " + HSC_OFF_FILE_PATH);
			}

			HSC_STREAM = HSC_VALUE.trim().replaceAll("\r\n", ",");
			/*Log.d(TAG, "HSC_STREAM = " + HSC_STREAM);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (HSC_STREAM != null) {
			Log.d(TAG, "setHscOn()");
			ExynosDisplayUtils.sysfsWriteSting(HSC_SYSFS_PATH, HSC_STREAM);
		}
	}

	public void setAtcValue(int value) {
		try {
			ATC_FILE_PATH = ATC_DIR_PATH.concat(Integer.toString(value));
			Log.d(TAG, "ATC_FILE_PATH = " + ATC_FILE_PATH);

			ATC_VALUE = ExynosDisplayUtils.getStringFromFile(ATC_FILE_PATH);

			ATC_STREAM = ATC_VALUE.trim().replaceAll("\r\n", ",");
			Log.d(TAG, "ATC_STREAM = " + ATC_STREAM);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (ATC_STREAM != null) {
			Log.d(TAG, "setAtcValue()");
			ExynosDisplayUtils.sysfsWriteSting(ATC_SYSFS_PATH, ATC_STREAM);
		}
	}

	public void setAtcReset() {
		try {
			ATC_VALUE = ExynosDisplayUtils.getStringFromFile(ATC_RESET_PATH);
			Log.d(TAG, "ATC_RESET_PATH = " + ATC_RESET_PATH);

			ATC_STREAM = ATC_VALUE.trim().replaceAll("\r\n", ",");
			Log.d(TAG, "atcStream = " + ATC_STREAM);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (ATC_STREAM != null) {
			Log.d(TAG, "setAtcReset()");
			ExynosDisplayUtils.sysfsWriteSting(ATC_SYSFS_PATH, ATC_STREAM);
		}
	}

	public void setAtcOn(int onoff) {
		try {
			if (onoff != 0) {
				ATC_VALUE = ExynosDisplayUtils.getStringFromFile(ATC_ON_FILE_PATH);
				Log.d(TAG, "ATC_ON_FILE_PATH = " + ATC_ON_FILE_PATH);
			} else {
				ATC_VALUE = ExynosDisplayUtils.getStringFromFile(ATC_OFF_FILE_PATH);
				Log.d(TAG, "ATC_OFF_FILE_PATH = " + ATC_OFF_FILE_PATH);
			}

			ATC_STREAM = ATC_VALUE.trim().replaceAll("\r\n", ",");
			/*Log.d(TAG, "ATC_STREAM = " + ATC_STREAM);*/
			ATC_ONOFF_VALUE = ExynosDisplayUtils.getStringFromFile(ATC_ONOFF_SYSFS_PATH);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (ATC_STREAM != null) {
			Log.d(TAG, "setAtcOn()");
			ExynosDisplayUtils.sysfsWriteSting(ATC_SYSFS_PATH, ATC_STREAM);
		}

		if ((!(ATC_ONOFF_VALUE == null)) && (!ATC_ONOFF_VALUE.equals("0"))) {
			Log.d(TAG, "setAtcOn(): onoff = " + onoff);
			ExynosDisplayUtils.sysfsWrite(ATC_ONOFF_SYSFS_PATH, onoff);
		}
	}

	public String getColorEnhancementMode() {
		try {
			MODE_NAME_VALUE = ExynosDisplayUtils.getStringFromFile(MODE_NAME_FILE_PATH);
			if (MODE_NAME_VALUE != null)
				MODE_NAME_STREAM = MODE_NAME_VALUE.trim().replaceAll("\r\n", ",");
			return MODE_NAME_STREAM;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setColorEnhancement(int value) {
		String cgc_value = null;
		String gamma_value = null;
		String hsc_value = null;
		String cgc_stream = null;
		String gamma_stream = null;
		String hsc_stream = null;

		try {
			switch (value) {
			case 0:
				cgc_value = ExynosDisplayUtils.getStringFromFile(MODE0_CGC_FILE_PATH);
				gamma_value = ExynosDisplayUtils.getStringFromFile(MODE0_GAMMA_FILE_PATH);
				hsc_value = ExynosDisplayUtils.getStringFromFile(MODE0_HSC_FILE_PATH);
				break;
			case 1:
				cgc_value = ExynosDisplayUtils.getStringFromFile(MODE1_CGC_FILE_PATH);
				gamma_value = ExynosDisplayUtils.getStringFromFile(MODE1_GAMMA_FILE_PATH);
				hsc_value = ExynosDisplayUtils.getStringFromFile(MODE1_HSC_FILE_PATH);
				break;
			case 2:
				cgc_value = ExynosDisplayUtils.getStringFromFile(MODE2_CGC_FILE_PATH);
				gamma_value = ExynosDisplayUtils.getStringFromFile(MODE2_GAMMA_FILE_PATH);
				hsc_value = ExynosDisplayUtils.getStringFromFile(MODE2_HSC_FILE_PATH);
				break;
			default:
			}
			if (cgc_value != null)
				cgc_stream = cgc_value.trim().replaceAll("\r\n", ",");
			if (gamma_value != null)
				gamma_stream = gamma_value.trim().replaceAll("\r\n", ",");
			if (hsc_value != null)
				hsc_stream = hsc_value.trim().replaceAll("\r\n", ",");
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (cgc_stream != null)
			ExynosDisplayUtils.sysfsWriteSting(CGC_SYSFS_PATH, cgc_stream);

		if (gamma_stream != null)
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, gamma_stream);

		if (hsc_stream != null)
			ExynosDisplayUtils.sysfsWriteSting(HSC_SYSFS_PATH, hsc_stream);
	}

	private void setClibData() {
		if (!ExynosDisplayUtils.existFile(CALIB_DATA_XML_PATH))
			return;

		String temp_array[] = null;

		try {
			temp_array = ExynosDisplayUtils.parserTuneXML(CALIB_DATA_XML_PATH, "tune", "dqe");

			if (temp_array == null)
				return;
			if (temp_array.length < 6) {
				Log.d(TAG, "xml array size wrong: " + temp_array.length);
				return;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (temp_array[3].equals("1"))
			ExynosDisplayUtils.sysfsWriteSting(CGC_SYSFS_PATH, temp_array[0]);
		else
			ExynosDisplayUtils.sysfsWriteSting(CGC_SYSFS_PATH, bypass_array[0]);

		if (temp_array[4].equals("1"))
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, temp_array[1]);
		else
			ExynosDisplayUtils.sysfsWriteSting(GAMMA_SYSFS_PATH, bypass_array[1]);

		if (temp_array[5].equals("1"))
			ExynosDisplayUtils.sysfsWriteSting(HSC_SYSFS_PATH, temp_array[2]);
		else
			ExynosDisplayUtils.sysfsWriteSting(HSC_SYSFS_PATH, bypass_array[2]);

		ExynosDisplayUtils.sendEmptyUpdate();
	}

	private void startTuneTimer() {
		if (bypass_array == null)
			bypass_array = ExynosDisplayUtils.parserXML(BYPASS_XML_FILE_PATH, "bypass", "dqe");

		if (mTuneTimer == null) {
			mTuneTimer = new Timer();
			mTuneTimer.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					setClibData();
				}
			}, mDelayMs, mPeriodMs);
		}
	}

	private void stopTuneTimer() {
		if (mTuneTimer != null) {
			mTuneTimer.cancel();
			mTuneTimer = null;
		}

		bypass_array = null;
	}

	protected void enableTuneTimer(boolean enable) {
		if (enable) {
			startTuneTimer();
		}
		else {
			stopTuneTimer();
		}
		Log.d(TAG, "enableTuneTimer: enable=" + enable);
	}
}
