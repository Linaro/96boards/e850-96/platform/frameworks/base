/*
 * Copyright (c) 2018 Samsung Electronics Co., Ltd.
 *
 * This software is proprietary of Samsung Electronics.
 * No part of this software, either material or conceptual may be copied or
 * distributed, transmitted, transcribed, stored in a retrieval system or
 * translated into any human or computer language in any form by any means,
 * electronic, mechanical, manual or otherwise, or disclosed to third parties
 * without the express written permission of Samsung Electronics.
*/

package android.telephony.ims.aidl;

/**
 * Provides APIs to indicate ePDG connection complete
 *
 * {@hide}
 */
import android.net.LinkProperties;

oneway interface IEPdgConnectionListener {
   void onRequestCompleted(in String name, in LinkProperties lp);
}