/*
 * Copyright (c) 2018 Samsung Electronics Co., Ltd.
 *
 * This software is proprietary of Samsung Electronics.
 * No part of this software, either material or conceptual may be copied or
 * distributed, transmitted, transcribed, stored in a retrieval system or
 * translated into any human or computer language in any form by any means,
 * electronic, mechanical, manual or otherwise, or disclosed to third parties
 * without the express written permission of Samsung Electronics.
*/

package android.telephony.ims.aidl;

import android.net.LinkProperties;
import android.net.NetworkCapabilities;

import android.telephony.ims.aidl.IEPdgConnectionListener;

/**
 * Provides APIs to open/close ePDG connection
 *
 * {@hide}
 */
oneway interface IEPdgConnectionInterface {
   void openConnection(in NetworkCapabilities nc, in IEPdgConnectionListener cb);
   void closeConnection(in LinkProperties lp, in IEPdgConnectionListener cb);
}