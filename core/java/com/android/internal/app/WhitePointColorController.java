/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.app;

import android.annotation.IntDef;
import android.annotation.NonNull;
import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.metrics.LogMaker;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.util.Slog;

import com.android.internal.R;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.logging.nano.MetricsProto.MetricsEvent;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeParseException;

/**
 * Controller for managing night display and color mode settings.
 * <p/>
 * Night display tints your screen red at night. This makes it easier to look at your screen in
 * dim light and may help you fall asleep more easily.
 */
public final class WhitePointColorController {

    private static final String TAG = "WhitePointColorController";
    private static final boolean DEBUG = true;

    private final Context mContext;
    private final int mUserId;
    private final ContentObserver mContentObserver;

    private Callback mCallback;
    private MetricsLogger mMetricsLogger;

    public WhitePointColorController(@NonNull Context context) {
        this(context, ActivityManager.getCurrentUser());
    }

    public WhitePointColorController(@NonNull Context context, int userId) {
        mContext = context.getApplicationContext();
        mUserId = userId;

        mContentObserver = new ContentObserver(new Handler(Looper.getMainLooper())) {
            @Override
            public void onChange(boolean selfChange, Uri uri) {
                super.onChange(selfChange, uri);

                final String setting = uri == null ? null : uri.getLastPathSegment();
                if (setting != null) {
                    onSettingChanged(setting);
                }
            }
        };
    }

    /**
     * Returns {@code true} when Night display is activated (the display is tinted red).
     */
    public boolean isActivated() {
        return Secure.getIntForUser(mContext.getContentResolver(),
                Secure.WHITE_POINT_DISPLAY_ACTIVATED, 0, mUserId) == 1;
    }

    /**
     * Sets whether White Point Color display should be activated. This also sets the last activated time.
     *
     * @param activated {@code true} if White Point Color display should be activated
     * @return {@code true} if the activated value was set successfully
     */
    public boolean setActivated(boolean activated) {
        if (!activated)
            setWhitePointColorLevel(getDefaultWhitePointColorLevel());
        return Secure.putIntForUser(mContext.getContentResolver(),
                Secure.WHITE_POINT_DISPLAY_ACTIVATED, activated ? 1 : 0, mUserId);
    }

    /**
     * Returns the White Point Color level to tint the display when activated.
     */
    public int getWhitePointColorLevel() {
        int colorLevel = Secure.getIntForUser(mContext.getContentResolver(),
                Secure.WHITE_POINT_DISPLAY_COLOR_LEVEL, -1, mUserId);
        if (colorLevel == -1) {
            if (DEBUG) {
                Slog.d(TAG, "Using default value for setting: "
                        + Secure.WHITE_POINT_DISPLAY_COLOR_LEVEL);
            }
            colorLevel = getDefaultWhitePointColorLevel();
        }

        return colorLevel;
    }

    /**
     * Sets the current White Point Color level.
     *
     * @param colorLevel.
     * @return {@code true} if new Skin Color Level was set successfully.
     */
    public boolean setWhitePointColorLevel(int level) {
        return Secure.putIntForUser(mContext.getContentResolver(),
                Secure.WHITE_POINT_DISPLAY_COLOR_LEVEL, level, mUserId);
    }

    /**
     * Returns the minimum allowed Color Temperature level to tint the display when activated.
     */
    public int getMinimumWhitePointColorLevel() {
        return 0; /*mContext.getResources().getInteger(
                R.integer.config_nightDisplayColorTemperatureMin);*/
    }

    /**
     * Returns the maximum allowed Color Temperature level to tint the display when activated.
     */
    public int getMaximumWhitePointColorLevel() {
        return 255; /*mContext.getResources().getInteger(
                R.integer.config_nightDisplayColorTemperatureMax);*/
    }

    /**
     * Returns the default Color Temperature level to tint the display when activated.
     */
    public int getDefaultWhitePointColorLevel() {
        return 127; /*mContext.getResources().getInteger(
                R.integer.config_nightDisplayColorTemperatureDefault);*/
    }

    private void onSettingChanged(@NonNull String setting) {
        if (DEBUG) {
            Slog.d(TAG, "onSettingChanged: " + setting);
        }

        if (mCallback != null) {
            switch (setting) {
                case Secure.WHITE_POINT_DISPLAY_ACTIVATED:
                    mCallback.onActivated(isActivated());
                    break;
                case Secure.WHITE_POINT_DISPLAY_COLOR_LEVEL:
                    mCallback.onLevelChanged(getWhitePointColorLevel());
                    break;
            }
        }
    }

    /**
     * Register a callback to be invoked whenever the Night display settings are changed.
     */
    public void setListener(Callback callback) {
        final Callback oldCallback = mCallback;
        if (oldCallback != callback) {
            mCallback = callback;

            if (callback == null) {
                // Stop listening for changes now that there IS NOT a listener.
                mContext.getContentResolver().unregisterContentObserver(mContentObserver);
            } else if (oldCallback == null) {
                // Start listening for changes now that there IS a listener.
                final ContentResolver cr = mContext.getContentResolver();
                cr.registerContentObserver(Secure.getUriFor(Secure.WHITE_POINT_DISPLAY_ACTIVATED),
                        false /* notifyForDescendants */, mContentObserver, mUserId);
                cr.registerContentObserver(Secure.getUriFor(Secure.WHITE_POINT_DISPLAY_COLOR_LEVEL),
                        false /* notifyForDescendants */, mContentObserver, mUserId);
            }
        }
    }

    private MetricsLogger getMetricsLogger() {
        if (mMetricsLogger == null) {
            mMetricsLogger = new MetricsLogger();
        }
        return mMetricsLogger;
    }

    /**
     * Returns {@code true} if Night display is supported by the device.
     */
    public static boolean isAvailable(Context context) {
        return true; /*context.getResources().getBoolean(R.bool.config_nightDisplayAvailable);*/
    }

    /**
     * Callback invoked whenever the Night display settings are changed.
     */
    public interface Callback {
        /**
         * Callback invoked when the activated state changes.
         *
         * @param activated {@code true} if Night display is activated
         */
        default void onActivated(boolean activated) {}

        /**
         * Callback invoked when the level changes.
         *
         * @param level the level to tint the screen
         */
        default void onLevelChanged(int level) {}
    }
}
