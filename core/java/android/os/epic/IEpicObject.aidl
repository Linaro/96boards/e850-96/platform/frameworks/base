/* //device/java/android/com/samsung/epic/IEpicManager.aidl
**
** Copyright 2007, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

package android.os.epic;

/** @hide */

interface IEpicObject
{
    boolean acquire_lock();
    boolean release_lock();
    boolean acquire_lock_option(int value, int usec);
    boolean acquire_lock_option_multi(in int[] value_list, in int[] usec_list);
    boolean perf_hint(String name);
    boolean hint_release(String name);
}
