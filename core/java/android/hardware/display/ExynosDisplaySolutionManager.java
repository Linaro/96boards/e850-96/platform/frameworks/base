    /*
     * Copyright (C) 2007 The Android Open Source Project
     * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *      http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

package android.hardware.display;

import android.os.RemoteException;
import android.util.Log;

/**
 * ExynosDisplaySolutionManager
 */
public final class ExynosDisplaySolutionManager {
	private static final String TAG = "ExynosDisplaySolutionManager";
	private static float RETURN_ERROR = -1;
	private static int RETURN_ERROR_INT = -1;
	final IExynosDisplaySolutionManager mService;

	/**
	 * @hide
	 */
	public ExynosDisplaySolutionManager(IExynosDisplaySolutionManager service) {
		mService = service;
	}

	private void onError(Exception e) {
		Log.e(TAG, "Error ExynosDisplaySolutionManager", e);
	}

	/**
	* Set a 'cabc mode value'
	* @param value One of the cabc mode
	*/
	public void setCABCModeSettingValue(int value) {
		try {
			mService.setCABCModeSettingValue(value);
		} catch (RemoteException e) {
			onError(e);
		}
	}

	/**
	* Get a 'String value' from EDS Service
	* @return String Value
	*/
	public String getColorEnhancementMode() {
		if (mService == null) {
			return null;
		}

		try {
			return mService.getColorEnhancementMode();
		} catch (RemoteException e) {
			onError(e);
		}
		return null;
	}

	/**
	* Set a 'color enhancement value'
	* @param value One of the color enhancement mode
	*/
	public void setColorEnhancementSettingValue(int value) {
		try {
			mService.setColorEnhancementSettingValue(value);
		} catch (RemoteException e) {
			onError(e);
		}
	}

	/**
	* Set a 'temp value'
	* @param value One of the temp
	*/
	public void setColorTempSettingValue(int value) {
		try {
			mService.setColorTempSettingValue(value);
		} catch (RemoteException e) {
			onError(e);
		}
	}

	/**
	* Set a 'onoff value'
	* @param onoff One of the temp
	*/
	public void setColorTempSettingOn(int onoff) {
		try {
			mService.setColorTempSettingOn(onoff);
		} catch (RemoteException e) {
			onError(e);
		}
	}

	/**
	* Set a 'temp value'
	* @param value One of the temp
	*/
	public void setEyeTempSettingValue(int value) {
		try {
			mService.setEyeTempSettingValue(value);
		} catch (RemoteException e) {
			onError(e);
		}
	}

	/**
	* Set a 'onoff value'
	* @param onoff One of the temp
	*/
	public void setEyeTempSettingOn(int onoff) {
		try {
			mService.setEyeTempSettingOn(onoff);
		} catch (RemoteException e) {
			onError(e);
		}
	}

	/**
	* Set a 'gain value'
	* @param r One of the gain
	* @param g One of the gain
	* @param b One of the gain
	*/
	public void setRgbGainSettingValue(int r, int g, int b) {
		try {
			mService.setRgbGainSettingValue(r, g, b);
		} catch (RemoteException e) {
			onError(e);
		}
	}

	/**
	* Set a 'onoff value'
	* @param onoff One of the gain
	*/
	public void setRgbGainSettingOn(int onoff) {
		try {
			mService.setRgbGainSettingOn(onoff);
		} catch (RemoteException e) {
			onError(e);
		}
	}

	/**
	* Set a 'onoff value'
	* @param onoff One of the color
	*/
	public void setSkinColorSettingOn(int onoff) {
		try {
			mService.setSkinColorSettingOn(onoff);
		} catch (RemoteException e) {
			onError(e);
		}
	}

	/**
	* Set a 'gain value'
	* @param h One of the gain
	* @param s One of the gain
	* @param v One of the gain
	*/
	public void setHsvGainSettingValue(int h, int s, int v) {
		try {
			mService.setHsvGainSettingValue(h, s, v);
		} catch (RemoteException e) {
			onError(e);
		}
	}

	/**
	* Set a 'onoff value'
	* @param onoff One of the gain
	*/
	public void setHsvGainSettingOn(int onoff) {
		try {
			mService.setHsvGainSettingOn(onoff);
		} catch (RemoteException e) {
			onError(e);
		}
	}

	/**
	 * Set a 'onoff value'
	 * @param onoff One of the color
	 */
	public void setWhitePointColorSettingOn(int onoff) {
		try {
			mService.setWhitePointColorSettingOn(onoff);
		} catch (RemoteException e) {
			onError(e);
		}
	}

	/**
	* Set a 'gain value'
	* @param r One of the gain
	* @param g One of the gain
	* @param b One of the gain
	*/
	public void setRgbGain(float r, float g, float b) {
		try {
			mService.setRgbGain(r, g, b);
		} catch (RemoteException e) {
			onError(e);
		}
	}

	/**
	* Get a 'float[] value' from EDS Service
	* @return float[] Value
	*/
	public float[] getRgbGain() {
		if (mService == null) {
			return null;
		}

		try {
			return mService.getRgbGain();
		} catch (RemoteException e) {
			onError(e);
		}
		return null;
	}

	/**
	 * Set a 'hbm mode value'
	 * @param value One of the hbm mode
	 */
	public void setHBMModeValue(int value) {
		try {
			mService.setHBMModeValue(value);
		} catch (RemoteException e) {
			onError(e);
		}
	}
}
