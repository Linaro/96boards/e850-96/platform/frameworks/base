/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.display;

/** @hide */
interface IExynosDisplaySolutionManager
{
	void setCABCModeSettingValue(int value);
	String getColorEnhancementMode();
	void setColorEnhancementSettingValue(int value);
	void setColorTempSettingValue(int value);
	void setColorTempSettingOn(int onoff);
	void setEyeTempSettingValue(int value);
	void setEyeTempSettingOn(int onoff);
	void setRgbGainSettingValue(int r, int g, int b);
	void setRgbGainSettingOn(int onoff);
	void setSkinColorSettingOn(int onoff);
	void setHsvGainSettingValue(int h, int s, int v);
	void setHsvGainSettingOn(int onoff);
	void setWhitePointColorSettingOn(int onoff);
	void setRgbGain(float r, float g, float b);
	float[] getRgbGain();
	void setHBMModeValue(int value);
}
