    /*
     * Copyright (C) 2007 The Android Open Source Project
     * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *      http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

package android.hardware.display;

import android.os.RemoteException;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.ServiceManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import java.nio.charset.StandardCharsets;

import java.util.List;
import java.util.ArrayList;

import android.util.Log;

/**
 * Exynos Display Color Implementation.
 * @hide
 */
public class ExynosDisplayColorImpl {
	private static final String TAG = "ExynosDisplayColorImpl";
	private final boolean DEBUG = "eng".equals(android.os.Build.TYPE);

	private final Object mLock = new Object();

	private String GAMMA_SYSFS_PATH = "/sys/class/dqe/dqe/gamma";
	private String CGC_SYSFS_PATH = "/sys/class/dqe/dqe/cgc";
	private String HSC_SYSFS_PATH = "/sys/class/dqe/dqe/hsc";

	private float[] rgb_gain = {1.0f, 1.0f, 1.0f};

	public ExynosDisplayColorImpl() {

	}

	private boolean sysfsWriteSting(String sysfs, String value) {
		FileOutputStream out = null;
		File myfile = new File(sysfs);
		if (myfile.exists()) {
			try {
				try {
					out = new FileOutputStream(myfile);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					return false;
				}
				out.write(value.getBytes());
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
				try {
					out.close();
				} catch (Exception err) {
					err.printStackTrace();
				}
				return false;
			}
			return true;
		}
		return false;
	}

	private void setRgbGainLocked(float r, float g, float b) {
		String stream = null;

		rgb_gain[0] = r;
		rgb_gain[1] = g;
		rgb_gain[2] = b;

		try {
			int[] gc = new int[65 * 3];
			StringBuilder stringBuilder = new StringBuilder();
			String[] gc_lut = new String[65 * 3];

			for (int i = 0; i < 65; i++) {
				gc[i] = (int)Math.round((double)(r * 255.0f) / 64 * i);
				gc[i+65] = (int)Math.round((double)(g * 255.0f) / 64 * i);
				gc[i+130] = (int)Math.round((double)(b * 255.0f) / 64 * i);
			}

			for (int i = 0; i < (65 * 3); i++)
				gc_lut[i] = Integer.toString(gc[i]);

			for (int i = 0; i < gc_lut.length; i++) {
				String temp_gc =  (i < (gc_lut.length -1)) ? (gc_lut[i] + ",") : (gc_lut[i]);
				stringBuilder.append(temp_gc);
			}

			if (stringBuilder.length() > 0)
				stream = stringBuilder.toString();

			/*Log.d(TAG, "gamma = " + stream);*/
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (stream != null) {
			Log.d(TAG, "setRgbGainLocked(): r=" + (r * 255.0f) + ", g=" + (g * 255.0f) + ", b=" + (b * 255.0f));
			sysfsWriteSting(GAMMA_SYSFS_PATH, stream);
		}
	}

	private float[] getRgbGainLocked() {
		return rgb_gain;
	}

	public void setRgbGain(float r, float g, float b) {
		synchronized (mLock) {
			setRgbGainLocked(r, g, b);
		}
	}

	public float[] getRgbGain() {
		synchronized (mLock) {
			return getRgbGainLocked();
		}
	}
}
